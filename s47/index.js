const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", printLastName);

txtLastName.addEventListener("keyup", printLastName);

function printLastName(event) {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}
